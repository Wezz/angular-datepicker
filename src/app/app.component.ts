import { Component, Input } from '@angular/core';

// Array of disabled dates
const disableDatesMockdata = [{year: 2020, month: 4, day: 24}, {year: 2020, month: 4, day: 15}];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'angular-datepicker';

  /**
  * The disableDates option
  */
  @Input() disableDates: any = disableDatesMockdata;
}
