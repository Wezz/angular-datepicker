# Angular Datepicker

We have developed a re-usable Angular datepicker which follows both our styling and functionality guidlines.

## Getting started

To setup a new Angular project using the Angular CLI

        npm install -g @angular/cli
        ng new my-dream-app
        cd my-dream-app
        ng serve

More information here - https://cli.angular.io/

## Step 1 - Install Node modules

The first thing you need to do is install these **two node modules:**

### Angular mydatepicker
We have used Angular mydatepicker for the typical datepicker functionality.
-     npm install angular-mydatepicker --save

### Datepicker component
This is our custom component with our desired functionality and styling.
For now you will need to install the Datepicker's node module locally until we have published the module.
-     npm i dist/datepicker/datepicker-0.0.1.tgz
     
### Import into main ngModule

    **
    import { AngularMyDatePickerModule } from 'angular-mydatepicker';
    import { DatepickerModule } from 'datepicker';

And of course reference them in the imports

     imports: [
        **,
        DatepickerModule,
        AngularMyDatePickerModule
      ],


## Step 2 - Add Datepicker's selector

Copy this markup into your html template, as you will notice there are a few properties you can change.

```
  <ui-datepicker
      [rangeOn]="true"
      [disableDates]="disableDates"
      label="Date range"
      placeholder="dd/mm/yyyy - dd/mm/yyyy">
  </ui-datepicker>
```


- **rangeOn**: Date picker mode (Single date picker or date range picker).
- **disableDates:** Set an array of date you would like to disable
- **label:** Label's text
- **placeholder:** Placeholders text which can be either be:
                   **dd/mm/yyyy** for a single date picker or **dd/mm/yyyy - dd/mm/yyyy** for a date range picker


 ### Setting up disabled days
    
- Add a new variable to your components .ts file above the @component decorator
- Add  @Input() disableDates: any; and equate it to this variable.

 ```
    // Array of disabled dates
    const disableDatesMockdata = [{year: 2020, month: 4, day: 24}, {year: 2020, month: 4, day: 15}];
    
    @Component({
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.scss']
    })
    
    
    export class AppComponent {
      title = 'angular-datepicker';
    
      /**
      * The disableDates option
      */
      @Input() disableDates: any = disableDatesMockdata;
    }
```
If you do not wish to have disabled days set an empty array.


## Keyboard accessibility

1. Tab the input into focus (at this point if you follow the dd/mm/yyyy format you can add the date by typing )
2. Tab again to focus on calendar icon now on space bar or enter you can open the calendar.
3. Pressing tab at this point will highlight the top left arrow and pressing enter will allow you to to navigate to previous month.
4. Tab again will highlight the month enter will take you into month selection.
5. Tab again will highlight the year enter will take you into year selection.
6. Tab again the next arrow to the next month
7. Tab again and you are in the calendar days and you can use the directional arrows to move in any direction and enter to select and date or range depending on the mode you have selected.
8. The calendar will close on section but you can use the esc key or tab back to the icon and enter to close.



## Year selection

Depending on the current year selected the view will show 12 years before and 12 years after the current selection.


# To use this repo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
